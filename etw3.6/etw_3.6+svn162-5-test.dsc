-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA512

Format: 3.0 (native)
Source: etw
Binary: etw, etw-data
Architecture: any all
Version: 3.6+svn162-5-test
Maintainer: Debian Games Team <pkg-games-devel@lists.alioth.debian.org>
Uploaders:  Markus Koschany <apo@debian.org>
Homepage: http://www.ggsoft.org/etw/
Standards-Version: 4.2.1
Vcs-Browser: https://salsa.debian.org/games-team/etw
Vcs-Git: https://salsa.debian.org/games-team/etw.git
Build-Depends: debhelper (>= 11), libgtk2.0-dev, libsdl1.2-dev
Package-List:
 etw deb games optional arch=any
 etw-data deb games optional arch=all
Checksums-Sha1:
 0a71e449f1c227e9e13eaa7b6e8f2fc1de0e4675 5832768 etw_3.6+svn162-5-test.tar.xz
Checksums-Sha256:
 976167c3e0837682439df9da0effd91547a49d97bb7ca1ac0c96178434067f31 5832768 etw_3.6+svn162-5-test.tar.xz
Files:
 dfeb6d4603f6599e2636e477bfea8e09 5832768 etw_3.6+svn162-5-test.tar.xz

-----BEGIN PGP SIGNATURE-----

iQGzBAEBCgAdFiEERTX4ZI1o6bOwVc8KKu+rGPw8lpUFAmR6+vgACgkQKu+rGPw8
lpU+xwv/cWL+XjJEI1BTXq/sG5Y2+xUXCzIWL4VtsZRV9OlgCKFMoHWdx8GdY9hY
nKHh6QKfx5JZ6NYxeb1C8PkxAlwX4JzQcL4tRz6GpWYIEmOp52r0nzT3JEtU/Ywv
6zJjTheJJ1CeNK0U6Hm/JFMcHomg8j7coMbzCP5sCmlaGozFRHrHF+qKXedwCA4Z
wJGc1B8WG5r3vZQczpHw7zJFu29CufFm9TD1Dm+MYB+o0a0/tow1u3A4YbfoL18A
qwc6rac4oUJGYDQpa/X4eRzTTST1d0Y/4Lm3FZJ0wDnLOMFbo46m19yxUIAcQrN3
pfEv759xhoFkTOaDQ9GBTtOKL6QxyhQr+HonUWlWoegTUCBN+jdyDC533INT75yo
FLR5jMc+Aw2/OSQAczHENjsDlFC+MO4vulxWm2ERQXoVYn0vHzHcWIdpn+ouukB/
ZYxxVisc2i6YFF9O/76Tiovk95E4jJxsEnaHWoXGUh6Sv6OcDpf3lWPa9CeDUcxZ
AQlabOW2
=vz79
-----END PGP SIGNATURE-----
