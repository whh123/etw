-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA512

Format: 3.0 (quilt)
Source: etw
Binary: etw, etw-data
Architecture: any all
Version: 3.6+svn162-5
Maintainer: Debian Games Team <pkg-games-devel@lists.alioth.debian.org>
Uploaders:  Markus Koschany <apo@debian.org>
Homepage: http://www.ggsoft.org/etw/
Standards-Version: 4.2.1
Vcs-Browser: https://salsa.debian.org/games-team/etw
Vcs-Git: https://salsa.debian.org/games-team/etw.git
Build-Depends: debhelper (>= 11), libgtk2.0-dev, libsdl1.2-dev
Package-List:
 etw deb games optional arch=any
 etw-data deb games optional arch=all
Checksums-Sha1:
 e396a5dce81168c4b17eb9c2450b11f9989ba766 9205100 etw_3.6+svn162.orig.tar.gz
 53caf057743f12066d03e966c18f7e1bef8f91c2 9368 etw_3.6+svn162-5.debian.tar.xz
Checksums-Sha256:
 147e7e0136d2411ed3155bc9f57aea3e6f3fd428e667265a4d19590d0a95cbc6 9205100 etw_3.6+svn162.orig.tar.gz
 84c376e0b4384e0a5d7c9f7b5ed94dfc157d4b70c05b76d3c56a1593182211c4 9368 etw_3.6+svn162-5.debian.tar.xz
Files:
 9acac9aa2d433ea9eb02df0a3f276640 9205100 etw_3.6+svn162.orig.tar.gz
 a98d47d40932fc831078981b78aca71c 9368 etw_3.6+svn162-5.debian.tar.xz

-----BEGIN PGP SIGNATURE-----

iQKjBAEBCgCNFiEErPPQiO8y7e9qGoNf2a0UuVE7UeQFAlzKACFfFIAAAAAALgAo
aXNzdWVyLWZwckBub3RhdGlvbnMub3BlbnBncC5maWZ0aGhvcnNlbWFuLm5ldEFD
RjNEMDg4RUYzMkVERUY2QTFBODM1RkQ5QUQxNEI5NTEzQjUxRTQPHGFwb0BkZWJp
YW4ub3JnAAoJENmtFLlRO1Hk5MsQAIyNd8wI2gdQL+JsOMNBpp5JcMXro2nkxuux
4fQlpPbHofMJPSNUW2GfghyM9VvWMD1xiciv0hajxI581+i6cfBXXQW9SBJCNmEb
t79RShZEZX4i+uDiUmhLKwd++lZ2qVIAvYKZccvlib/I/Q6naQnb6j2SP4SMm3LW
vMwJbAk7pz0a1+wgXW+TG2g87BKKTRhlcDM9xl/6qIpYNNu6tGs2bZqqpDZnxwT+
uvqSe3cJsQVUOIkpxNYx5vfGRViJ1JBrnQaw5dCXm+ztWECMYQPtEV6IvPk60ast
V7kszMIngmBFoKlKj6TS5VNqp538tysT+gn44C0s+DZTLj5Eya9ibmlaLqwDKmTw
8knBPTIStOrrVDiFYMKG5TRDZvWKsz6f9ZOR6kIU3CX7Q3fyBHnY4+CQIq3X885C
K9i0zxhca6pBOnHYI0H5iyf/nTRALGKztvHL1VgBNSR1qgdGKnWJWua3chbtEb+k
zUMQmcfftIvcFEpeJyoKOr4fNscMI0JQSfZtJpNGGPdMf+Xh893KZFV2GithqS8c
18s0hggGwofGYdo8uVh0HXpRrZOV/jnQKp62AtgqRRV0dKUs5T699VRuVJKTpBq6
EA8lEro6yaO0ueoVzuRnwjahlalQIIfwsQNFPVG8RrzFeDfWHp4qRvu0KPMGCF+I
KzZBmnQu
=7enl
-----END PGP SIGNATURE-----
